<?php

namespace App\Docsets;

use Godbout\DashDocsetBuilder\Docsets\BaseDocset;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Wa72\HtmlPageDom\HtmlPageCrawler;

class Drupal extends BaseDocset
{
    private const VERSION = '9.1.x';

    public const CODE = 'drupal';
    public const NAME = 'Drupal 9.1.x';
    public const URL = 'api.drupal.org';
    public const INDEX = 'api/drupal/9.1.x.html';
    public const PLAYGROUND = '';
    public const ICON_16 = '../../icons/icon.png';
    public const ICON_32 = '../../icons/icon@2x.png';
    public const EXTERNAL_DOMAINS = [];

    public function grab(): bool
    {
        $version = preg_quote(self::VERSION);
        $accept = implode('|', [
            '\.css$',
            '\.ico$',
            '\.png$',
            '\.gif$',
            '\.svg$',
            '/api/drupal/' . $version . '$',
            '/api/drupal/(autoload|index|update)\.php/' . $version . '$',
            '/api/drupal/(core|vendor|composer)[^/]+\.(php|inc|install|module|profile|theme|twig)/.*' . $version . '$',
            '/api/drupal/(groups|classes|functions|files|namespaces|services|elements|constants|globals|deprecated)/' . $version . '(\?page=[0-9]+)?$',
            '/api/drupal/namespace/[^/]+/' . $version . '$',
        ]);

        $index_url = str_replace('.html', '', $this->index());

        system(
            "echo; wget {$index_url} \
                --mirror \
                --no-clobber \
                --trust-server-names \
                --accept-regex='{$accept}' \
                --reject-regex='VnPBBfwe' \
                --ignore-case \
                --page-requisites \
                --adjust-extension \
                --convert-links \
                --directory-prefix=storage/{$this->downloadedDirectory()} \
                -e robots=off \
                --quiet \
                --show-progress",
            $result
        );

        return $result === 0;
    }

    public function entries(string $file): Collection
    {
        $crawler = HtmlPageCrawler::create(Storage::get($file));

        $entries = collect();
        $matches = [];
        $version = preg_quote(self::VERSION);
        $pattern = <<< "PATTERN"
          @
            /drupal/(?<filename>[^/]*)/((?<type>constant|class|function|global|group|interface|property|service|trait)/(?<name>[^/]*)/)?$version
          @x
        PATTERN;

        if (preg_match($pattern, $file, $matches, PREG_UNMATCHED_AS_NULL)) {
          ['filename' => $filename, 'type' => $type, 'name' => $name] = $matches + ['type' => NULL, 'name' => NULL];

          switch ($type) {
          case 'global':
          case 'service':
              break;

          case 'interface':
          case 'property':
          case 'trait':
          case 'class':
              $name = $this->addNamespace($name, $crawler);
              break;

          case 'constant':
              if (strpos($name, '::') !== FALSE) {
                  $name = $this->addNamespace($name, $crawler);
              }
              break;

          case 'group':
              $type = 'Guide';
              $name = $crawler->filter('#page-subtitle')->text('Drupal');
              break;

          case 'function':
              if (strpos($name, 'hook_') === 0) {
                  $type = 'Hook';
              }
              elseif (strpos($name, 'callback_') === 0) {
                  $type = 'Callback';
              }
              elseif (strpos($name, '::') !== FALSE) {
                  $type = 'Method';
                  $name = $this->addNamespace($name, $crawler);
              }
              break;

          default:
              $type = 'File';
              $name = str_replace('!', '/', $filename);
              // Exclude bare directory names.
              if (strpos($name, '.') === FALSE && strpos($name, '/') === FALSE) {
                return $entries;
              }
          }

          $entries->push([
              'name' => $name,
              'type' => ucfirst($type),
              'path' => Str::after($file, $this->innerDirectory())
          ]);

        }

        return $entries;
    }

    protected function addNamespace(string $name, HtmlPageCrawler $crawler): string
    {
        $namespace = $crawler->filter('[href*="/api/drupal/namespace/"]')->text('');
        return $namespace . '\\' . $name;
    }

    public function format(string $file): string
    {
        $crawler = HtmlPageCrawler::create(Storage::get($file));

        $crawler->filter('#skip-link')->remove();
        $crawler->filter('#nav-header')->remove();
        $crawler->filter('#header')->remove();
        $crawler->filter('#content .content > h2.element-invisible')->remove();
        $crawler->filter('#content .content > ul.tabs.primary')->remove();
        $crawler->filter('#aside')->remove();
        $crawler->filter('#footer')->remove();

        // Set the tab title to something simpler.
        $crawler->filter('title')->setInnerHtml($crawler->filter('#page-subtitle')->text('Drupal'));

        // Remove all scripts.
        $crawler->filter('script')->remove();

        // Use proper <details> elements, this ain't the 90s.
        $crawler->filter('.ctools-collapsible-container')->each(function ($crawler) {
          $summary = $crawler->filter('.ctools-collapsible-handle')->text('');
          $details = $crawler->filter('.ctools-collapsible-content')->html();

          $crawler->replaceWith('<details><summary>' . $summary . '</summary>' . $details . '</details>');
        });

        $styles = <<< 'STYLES'
            details {
                margin-bottom: 1.385em;
            }
            details > summary {
                font-weight: bold;
                margin-bottom: 0.692em;
            }
            details > summary::-webkit-details-marker {
                display: inline-block;
            }
        STYLES;
        $crawler->filter('head')->append('<style>' . $styles . '</style>');

        // Remove sort links in table headers.
        $crawler->filter('th > a')->each(function ($crawler) {
          $crawler->replaceWith($crawler->text());
        });

        return $crawler->saveHTML();
    }
}

